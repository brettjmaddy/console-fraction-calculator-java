
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class FractionCalc 
{    
    public static void main(String[] args)
    {        
        Fraction fraction1;  //initializing the first Fraction object to be used.
        Fraction fraction2;         //intiializing the second Fraction object to be used.
        String fractionToken;  //this string will be used to store fraction tokens identified by the Matcher.find()
        String[] fraction1Array;  //this array will store the numbers-as-strings of the first fraction split from the fractionToken string
        String[] fraction2Array;  //this array will store the numbers-as-strings of the first fraction split from the fractionToken string
        int[] parsedIntsArray = new int[4];  //this array will hold the integers parsed from the string arrays, to be used as fraction object arguments. 
        Fraction calcResult;   //initializing the Fraction object that will store the return value of the calculation methods to be printed.
        Matcher userInputMatcher;   //initializing a Matcher object to used for finding regex groupings.
        ArrayList<String> fractionList = new ArrayList<>();  //ArrayList to store individually split number-strings from the stringToken. Used for the convenince of .add()
        
        String twoFractRegex = "-?[0-9]+[ \t]*/[ \t]*-?[0-9]+[ \t]*[+-/\\*][ \t]*-?[0-9]+[ \t]*/[ \t]*-?[0-9]+"; //regex for validating user input.               
        String fractRegex = "-?[0-9]+[ \t]*/[ \t]*-?[0-9]+"; //regex usex to find a single fraction string in the Matcher.        
        
        System.out.println("Please enter two positive or negative fractions you wish to calculate.");
        System.out.println("They should be in the form: (-)n1 / (-)n2 + (-)n3 / (-)n4 (spaces optional)");
        System.out.println("You may enter whichever mathematical operation you wish in place of the \"+\"\n");
        
        Scanner input = new Scanner(System.in);
        String equation = input.nextLine();
        equation = equation.replace(" ", "");   //trimming the input string to remove spaces
        
        //eliminitaing bad input       
        if(!(equation.matches(twoFractRegex)))
        {
            System.err.println("Your input does not match the prescribed format. Please try again");
            System.exit(0);
        }
        
        //using the full twoFractRegex seems to cause ArrayIndexOutOfBounds. A single fraction regex doesn't cause runtime errors. Not quite sure why.
        userInputMatcher = Pattern.compile(fractRegex).matcher(equation); 
            
        while(userInputMatcher.find())
        {   
            fractionToken = userInputMatcher.group(); //finding tokens that match the regex.
            fractionList.add(fractionToken); //adding the tokens to the ArrayList.
            equation = equation.replace(fractionToken, "");  //though perhaps a little clunky, this will replace the matching token with "" to get rid of it, leaving just the math operator.
        }
            
        //splitting the fraction string elements in the arraylist to put individual numbers into arrays.
        fraction1Array = fractionList.get(0).split("/");
        fraction2Array = fractionList.get(1).split("/");
            
        //parsing the integers from strings out of the arrays
        parsedIntsArray[0] = Integer.parseInt(fraction1Array[0]);        
        parsedIntsArray[1] = Integer.parseInt(fraction1Array[1]);
        parsedIntsArray[2] = Integer.parseInt(fraction2Array[0]);
        parsedIntsArray[3] = Integer.parseInt(fraction2Array[1]);
            
        //eliminating division by 0
        if(parsedIntsArray[1] == 0 || parsedIntsArray[3] == 0)
        {
            System.err.println("You have a 0 in a denominator! Can't calculate.");
            System.exit(0);
        }
            
        //there is an anomaly when subrating a postive number. the equation string is emptied before I want it to be.
        //if the equation string is empty and the second fraction's numerator is negative, I know that the "-" operator was paired with
        //the nuext numerator as if it was a negative number
        if(equation.equals("") && parsedIntsArray[2] < 0)
        {
            equation = "-";
            parsedIntsArray[2] = parsedIntsArray[2] * -1;          
        }  
            
        fraction1 = new Fraction(parsedIntsArray[0], parsedIntsArray[1]);
        fraction2 = new Fraction(parsedIntsArray[2], parsedIntsArray[3]);         
        
        //switch block to determine the mathematical operation that should be performed.
        switch(equation)
        {
            case "+":            
                calcResult = fraction1.add(fraction2);            
                System.out.println(calcResult.toString());
                break;            
                
            case "-":                
                calcResult = fraction1.sub(fraction2);
                System.out.println(calcResult.toString());
                break;
                
            case "*":                
                calcResult = fraction1.mult(fraction2);                
                System.out.println(calcResult.toString());
                break;
                
            case "/":                
                calcResult = fraction1.div(fraction2);                
                System.out.println(calcResult.toString());
                break;
        }      
    }                
}

