
public class Fraction 
{
    private int numerator;
    private int denominator;
    
    public Fraction(int numerator, int denominator)
    {
        int divisor = gcd(numerator, denominator);
        this.numerator = numerator / divisor;
        this.denominator = denominator / divisor;
    }
    
    public Fraction(int numerator)
    {
        this.numerator = numerator;
        this.denominator = 1;
    }
    
    public Fraction add(Fraction right)
    {
        int newThisNumerator;
        int newRightNumerator;
        int addedNumerators;
        int commonDenominator;
        if(this.denominator == right.denominator)
        {
            commonDenominator = denominator;
            addedNumerators = this.numerator + right.numerator;
        }
        else
        {
            commonDenominator = this.denominator * right.denominator;
            newThisNumerator = right.denominator * this.numerator;
            newRightNumerator = this.denominator * right.numerator;
            addedNumerators = newThisNumerator + newRightNumerator;
        }
        return new Fraction(addedNumerators, commonDenominator);
    }
    
    public Fraction sub(Fraction right)
    {
        int newThisNumerator;
        int newRightNumerator;
        int subbedNumerators;
        int commonDenominator;
        
        if(this.denominator == right.denominator)
        {
            commonDenominator = denominator;
            subbedNumerators = this.numerator - right.numerator;            
        }
        else
        {
            commonDenominator = this.denominator * right.denominator;
            newThisNumerator = right.denominator * this.numerator;
            newRightNumerator = this.denominator * right.numerator;
            subbedNumerators = newThisNumerator - newRightNumerator;
        }
        
        
        return new Fraction(subbedNumerators, commonDenominator);
    }
    
    public Fraction mult(Fraction right)
    {
        return new Fraction(this.numerator * right.numerator, this.denominator * right.denominator);
    }
    
    public Fraction div(Fraction right)
    {       
        return new Fraction(this.numerator * right.denominator, this.denominator * right.numerator);
    }
    
    public String toString()
    {
        return numerator + "/" + denominator;
    }
    
    public boolean equals(Object otherObject)
    {
        if(this == otherObject)
        {
            return true;
        }
        
        if(otherObject == null)
        {
            return false;
        }
        
        if(getClass() != otherObject.getClass())
        {
            return false;
        }
        
        Fraction other = (Fraction)otherObject;
        
        return numerator == other.numerator && denominator == other.denominator;
    }
    
    private int gcd(int u, int v)
    {
        u = (u < 0) ? -u : u;
        v = (v < 0) ? -v : v;
        
        while(u > 0)
        {
            if(u < v)
            {
                int t = u;
                u = v;
                v = t;
            }
            
            u -= v;
        }
        
        return v;        
    }
}
